
const config =  {
  ordersLimit: 100, // Maximum number of records returned
  databaseFile: 'northwind.db'
}

module.exports = config;

// SQLite database access

// SQLite Node.js tutorial - https://www.sqlitetutorial.net/sqlite-nodejs/
// sqlite3 - https://www.npmjs.com/package/sqlite3?activeTab=readme
// sqlite3 API - https://github.com/TryGhost/node-sqlite3/wiki/API

// TODO: Disable verbose from production.
const sqlite3 = require('sqlite3').verbose();
const config = require('../config');

const db = new sqlite3.Database('./sqlite-db/' + config.databaseFile, sqlite3.OPEN_READONLY, (err) => {
  if (err) {
    return console.error(err.message);
  }
  console.log('Connected to the northwind database.');
});

module.exports = db;

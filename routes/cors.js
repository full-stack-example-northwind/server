// Cross-Origin Resource Sharing (CORS)

const express = require('express');
const cors = require('cors');
const app = express();

const whitelist = [
        'http://localhost:3000', 'https://localhost:3443',
        'http://127.0.0.1:3000', 'https://127.0.0.1:3443',
        'http://127.0.0.1:5858', 'https://127.0.0.1:5858'
    ];
var corsOptionsDelegate = (req, callback) => {
    var corsOptions;
    console.log("** header Origin: ", req.header('Origin'));
    // accept (options?) requests incoming from whitelisted origins
    if(whitelist.indexOf(req.header('Origin')) !== -1) {
        corsOptions = { origin: true, preflightContinue: false }; // reflect (enable) the requested origin in the CORS response
    }
    else {
        corsOptions = { origin: false, preflightContinue: false }; // disable CORS for this request 
    }
    callback(null, corsOptions); // callback expects two parameters: error and options 
};

exports.cors = cors(); // cors() - Will reply back with   Access-Control-Allow-Origin: *
exports.corsWithOptions = cors(corsOptionsDelegate);

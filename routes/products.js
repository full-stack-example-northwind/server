var router = require('express').Router();
const cors = require('./cors'); // CORS related routes
const db = require('./db-access');


const select = 'SELECT ProductID, ProductName FROM Products';
const where = ' WHERE ProductID = ?'
const orderby = ' ORDER BY ProductName';

/* GET Product list */
router.get('/:productID', cors.corsWithOptions, (req, res, next) => {
  const sql = select + where;

  db.all(sql, req.params.productID, (err, rows) => {
    if (err) {
      next(err);
    } else {
      res.json(rows);
    }
  });
});

router.get('/', cors.corsWithOptions, (req, res, next) => {
  const sql = select + orderby;
  
  db.all(sql, [], (err, rows) => {
    if (err) {
      next(err);
    } else {
      res.json(rows);
    }
  });
});
  
module.exports = router;
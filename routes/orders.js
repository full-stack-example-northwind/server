const router = require('express').Router();
const cors = require('./cors'); // CORS related routes
const db = require('./db-access');
const async = require('async');
const config = require('../config');

const columns = ' OrderID, ShippedDate, ShipAddress, ShipCity, ShipRegion, ShipPostalCode, ShipCountry, ContactName, NULL as Products';
const joinCustomers = ' JOIN Customers USING(CustomerID)';
const orderBy = ' ORDER BY OrderID DESC';
const pagination = ' LIMIT ? OFFSET ?';

// GET -- Route /orders/
//     -- Route /orders/?productID=id
//     -- Route /orders/?productID=id&limit=10&start=15
router.get('/', cors.corsWithOptions, (req, res, next) => {
  let sql;
  let sqlParams = [];

  // -- Pagination req.query parameters --
  // * limit  - max size of the result set
  // * start  - offset in the result set
  let limit = config.ordersLimit;
  let offset = isNaN(req.query.start) ? 0 : parseInt(req.query.start);
  if (!isNaN(req.query.limit)) {
    limit = Math.min(limit, Math.max(req.query.limit, 3)); // in [3 ... ordersLimit]
  }

  let countSql;
  if (!req.query.ProductID) {
    // All orders
    countSql = 'SELECT count(*) as rows FROM Orders';
    sql = 'SELECT ' + columns + ' FROM Orders' + joinCustomers + orderBy;
  } else {
    // All orders having spesific product 
    countSql = 'SELECT count(*) as rows FROM "Order Details" WHERE ProductID = ' + req.query.ProductID;
    sql = 'SELECT ' + columns + ' FROM "Order Details" JOIN Orders USING(OrderID)' 
      + joinCustomers + ' WHERE ProductID = ?' + orderBy;
    sqlParams = [req.query.ProductID];
  }
  
  sql += pagination; // Add pagination query
  sqlParams = [ ...sqlParams, limit, offset]; // Add pagination parameters

  // Total row count query followed by the actual query
  db.get(countSql, [], (err, count) => {
    if (err) {
      next(err);
      console.error(err);
    } else {
      getOrders_Products(sql, sqlParams, (rows) => {
        const response = {
          pagination: {
            totalCount: count.rows,
            limit: limit,
            start: offset
          },
          orders: rows 
        }
        res.json(response);
      });
    }
  });
});

/* GET Order by OrderID */
router.get('/:orderID', cors.corsWithOptions, (req, res, next) => {
  const sql = 'SELECT ' + columns + ' FROM Orders' + joinCustomers + ' WHERE OrderID = ?';
  const sqlParams = req.params.orderID;

  getOrders_Products(sql, sqlParams, (rows) => {
    res.json(rows);
  });
});

// returnRows - callback, returns Orders rows augmented with the Products information.
getOrders_Products = (getOrdersSQL, queryParams, returnRows) => {
  const productsSQL = 'SELECT ProductID, ProductName FROM Products JOIN "Order Details" USING(ProductID) WHERE OrderID = ?';

  db.all(getOrdersSQL, queryParams, (err, rows) => {
    if (err) {
      console.error(err);
      returnRows([]);
    } else {
      // For each order row, augment it with an array of products.      
      async.forEach(rows, (row, reportBack) => { // https://www.npmjs.com/package/async
        db.all(productsSQL, row.OrderID, (err, products) => {
          if (err) {
            // If subquery fails, return empty array of products
            console.error(err);
            row.Products = [];
            reportBack(err);
          }
          else {
            row.Products = products;
            reportBack();
          }
        });
      }, (err) => {
        // Called when all subqueries are done
        if (err) {
          console.error(err);
          returnRows([]);
        }
        returnRows(rows);
      });
    }
  });
};

module.exports = router;

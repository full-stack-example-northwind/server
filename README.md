# Orders Search API

# Version history

## Released
### 1.0
Initial version (see Usage sections below).

## Planned
### 1.1
* Support for HTTPS.

# Introduction

The backend major functionality can be summarized as showing list of orders filtered by a product.

## Usage

The backend offers its RESTFull API using routes. The API:
* /products/ returns a distinct list of all product names and ids. 

* /orders/ returns a list of all matching orders. If wrong type of query parameters is given, default values are used:
  * ProductID - (number, optional) product id to find matching orders. If not provided, all orders are returned. If product id does not exist in database, empty array of orders is returned. 
  * limit - (number, optional, default: 100) maximum number of orders returned. If given limit is larger than configured on server side, then default value is used.
  * start - (number, optional, default: 0) offset of the pagination.


## Database

The database used by the backend is North Wind DB, available as part of the souce code (downloaded from https://github.com/jpwhite3/northwind-SQLite - see the database model 
(https://lite.datasette.io/?url=https%3A%2F%2Fraw.githubusercontent.com%2Fjpwhite3%2Fnorthwind-SQLite3%2Fmaster%2Fdist%2Fnorthwind.db).


# Design desicion
 n. a.

# Known issues 
n. a.

# Technology stack
The API is implemented using:
* `ECMAScript 6 (2015)`.
* `Express.js 4`

## Setup history
  * `npx express-generator server` # generate server side API server, using express-generator
  * `npm i sqlite3`   # enable database access 
  * `npm i cors`      # enable Cross-Origin Resource Sharing (CORS)
  * `npm i asyc`      # needed to make nested asynchronous database queries to work

# CI
This section is on how to further develop (build and deploy) the code.

## Prerequisites
* `NodeJS 18` (with `NPM 9`).
* IDE that support JavaScript (e.g. VSCode version: 1.76 or newer)
* SQLite DB Viewer/Explorer to look into the DB (e.g. VSCode SQLITE Explorer extension - it needs sqlite3 binaries in the system (OS) path)

## Available Scripts

After having the prerequisites installed, and checking out the project from Git repository, in the project directory, you can run:

### `npm ci`

Downloads the Node modules required, builds the code. 

### `npm start`

Runs the app in the development mode under [http://localhost:8080](http://localhost:8080).

You may also see any lint errors in the console.

### `npm test`

Launches the test runner.

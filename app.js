var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
//app.use(express.static(path.join(__dirname, 'public')));

const database = require('./routes/db-access');

const ordersRouter = require('./routes/orders');
const productsRouter = require('./routes/products');



function makeErr(res) {
  var err = new Error('You are not authenticated!');
  res.setHeader('WWW-Authenticate', 'Basic'); // Challenge the client to authenticate itself
  err.status = 401; // Unauthorized access
  return err;
}

app.use('/orders', ordersRouter);
app.use('/products', productsRouter);

/**
 * On shutdown, close db connection
 * For more on Signal events, see: https://nodejs.org/api/process.html#signal-events
 */
const terminationHandler = (signal) => {
  console.log(`\nReceived ${signal}. -- Exiting...`);
  process.exit(signal);
}

process.on('SIGTERM', terminationHandler);
process.on('SIGINT', terminationHandler);

process.on('exit', (code) => {
  console.log('Closing database...');
  database.close((err) => {
    if (err) {
      return console.error(err.message);
    }
    console.log('Database connection closed.');
  });
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send(err);
});

module.exports = app;
